# Command to build:
# "docker build -t registry.gitlab.com/primer13r/php:7.3-fpm-cassandra -f ./php_7.3-fpm-cassandra.Dockerfile ."
# Command to push:
# "docker push registry.gitlab.com/primer13r/php:7.3-fpm-cassandra"
#
FROM registry.gitlab.com/primer13r/php:7.3-fpm
LABEL maintainer="primer.caballero@gmail.com"

# Install dependencies for datastax php-driver for cassandra
RUN buildDeps=" \
    " \
    runtimeDeps=" \
        build-essential \
        cmake \
        git \
        libpcre3-dev \
        libgmp-dev \
        libuv1-dev \
        libssl-dev \
    " \
    && apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y $buildDeps $runtimeDeps \
    && apt-get purge -y --auto-remove $buildDeps \
    && rm -r /var/lib/apt/lists/*

# # Install DataStax C/C++ Driver
# WORKDIR /root
# RUN git clone https://github.com/datastax/cpp-driver.git cpp-driver \
#     && cd cpp-driver \
#     && git checkout tags/2.13.0 \
#     && mkdir build \
#     && cd build \
#     && cmake -DCASS_BUILD_STATIC=ON -DCASS_BUILD_SHARED=ON .. \
#     && make -j4 \
#     && make install \
#     && rm -rf /root/cpp-driver \
#     && ln -s /usr/local/lib/x86_64-linux-gnu/libcassandra* /usr/local/lib/

# Install Cassandra PHP extension
WORKDIR /root
RUN git clone https://github.com/datastax/php-driver.git \
	&& cd php-driver \
    && git submodule update --init \
    && cd lib/cpp-driver && mkdir build && cd build && cmake .. && make && make install && cd ../../../ \
    && cd ext && phpize && cd .. \
    && mkdir build && cd build && ../ext/configure && make && make install && cd .. \
    && rm -rf /root/php-driver