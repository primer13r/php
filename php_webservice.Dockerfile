# Command to build:
# "docker build -t registry.gitlab.com/primer13r/php -f ./php73-mio.Dockerfile ."
#
#FROM php:7.3-fpm
FROM chialab/php:7.3-fpm
LABEL maintainer="primer.caballero@gmail.com"

# Install PHP extensions and PECL modules.
# RUN buildDeps=" \
#         default-libmysqlclient-dev \
#         libbz2-dev \
#         libmemcached-dev \
#         libsasl2-dev \
#     " \
#     runtimeDeps=" \
#         curl \
#         git \
#         libfreetype6-dev \
#         libicu-dev \
#         libjpeg-dev \
#         libldap2-dev \
#         libmemcachedutil2 \
#         libpng-dev \
#         libpq-dev \
#         libxml2-dev \
#         libzip-dev \
#     " \
#     && apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y $buildDeps $runtimeDeps \
#     && docker-php-ext-install bcmath bz2 calendar iconv intl mbstring mysqli opcache pdo_mysql pdo_pgsql pgsql soap zip \
#     && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
#     && docker-php-ext-install gd \
#     && docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ \
#     && docker-php-ext-install ldap \
#     && docker-php-ext-install exif \
#     && pecl install memcached redis \
#     && docker-php-ext-enable memcached.so redis.so \
#     && apt-get purge -y --auto-remove $buildDeps \
#     && rm -r /var/lib/apt/lists/*
# 
# Install Composer.
# RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
#     && ln -s $(composer config --global home) /root/composer
# ENV PATH=$PATH:/root/composer/vendor/bin COMPOSER_ALLOW_SUPERUSER=1

ENV ACCEPT_EULA=Y

# Microsoft SQL Server Prerequisites
RUN apt-get update \
	&& apt-get install -y gnupg2 \
    && curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - \
    && curl https://packages.microsoft.com/config/debian/9/prod.list \
        > /etc/apt/sources.list.d/mssql-release.list \
    && apt-get install -y --no-install-recommends \
        locales \
        apt-transport-https \
    && echo "en_US.UTF-8 UTF-8" > /etc/locale.gen \
    && locale-gen \
    && apt-get update \
    && apt-get -y --no-install-recommends install \
        unixodbc-dev \
        msodbcsql17

RUN pecl install sqlsrv pdo_sqlsrv xdebug \
	&& docker-php-ext-enable sqlsrv pdo_sqlsrv xdebug
	
# Install bcmath extension
RUN docker-php-ext-install bcmath
	
# Install unzip (To avoid issues in installations of some composer packages)
RUN apt install -y unzip


# Install dependencies for datastax php-driver for cassandra
RUN apt-get -y install build-essential cmake git libpcre3-dev libgmp-dev libuv1-dev libssl-dev

# Install DataStax C/C++ Driver
WORKDIR /root
RUN git clone https://github.com/datastax/cpp-driver.git cpp-driver \
    && cd cpp-driver \
    && git checkout tags/2.13.0 \
    && mkdir build \
    && cd build \
    && cmake -DCASS_BUILD_STATIC=ON -DCASS_BUILD_SHARED=ON .. \
    && make -j4 \
    && make install

RUN ln -s /usr/local/lib/x86_64-linux-gnu/libcassandra* /usr/local/lib/
RUN rm -rf /root/cpp-driver

# Install Cassandra PHP extension
WORKDIR /root
RUN git clone https://github.com/datastax/php-driver.git \
	&& cd php-driver \
	&& git submodule update --init \
	&& cd ext \
	&& ./install.sh
	
RUN rm -rf /root/php-driver

#RUN pecl install cassandra-1.3.2  \
#    && docker-php-ext-enable cassandra \
#    && rm -rf /tmp/pear \
#    && php -m | grep cassandra

